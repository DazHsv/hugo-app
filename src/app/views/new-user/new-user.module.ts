import { NewUserComponent } from './new-user.view.component';
import { NgModule } from "@angular/core";
import { UserFormModule } from '../../components/user-form/user-form.module';

@NgModule({
    imports: [UserFormModule],
    declarations: [NewUserComponent],
    exports: [NewUserComponent]
})
export class NewUserModule {}
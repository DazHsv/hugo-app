import { PopupAlertService } from './../../services/popup-alert.service';
import { UserService } from './../../services/user.service';
import { User } from './../../models/User';
import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs/Observable";

@Component({
    selector: 'user',
    templateUrl: './users.view.component.html'
})
export class UsersComponent implements OnInit {
    users$: Observable<User[]>;

    constructor(
        private userService: UserService,
        private alertService: PopupAlertService) {}

    ngOnInit() {
        this.users$ = this.userService.getUsers();
    }

    onDelete(id: number) {
        this.userService.deleteUser(id)
            .subscribe(() => {
                this.alertService.createAlert({
                    message: 'User successfully deleted!',
                    type: 'success'
                });
            });
    }
}
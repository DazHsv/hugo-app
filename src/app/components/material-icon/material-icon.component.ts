import { Component, Input } from "@angular/core";

@Component({
    selector: '[m-icon]',
    templateUrl: './material-icon.component.html'
})
export class MaterialIconComponent {
    @Input('m-icon') icon: string;
    @Input('m-icon-text') text: string;
}
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { UserFormComponent } from './user-form.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [CommonModule, RouterModule, ReactiveFormsModule],
    declarations: [UserFormComponent],
    exports: [UserFormComponent]
})
export class UserFormModule {}